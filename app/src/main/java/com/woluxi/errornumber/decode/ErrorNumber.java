/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber.decode;

/**
 * Created by ninadhg on 1/24/17.
 */

public class ErrorNumber {
    private String errorCode;
    private String errorInfo;
    private int number;

    public ErrorNumber(String errorCode, String errorInfo, int number) {
        this.errorCode = errorCode;
        this.errorInfo = errorInfo;
        this.number = number;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorInfo() {
        return errorInfo;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public String toString() {
        return  errorInfo +
                "(" + errorCode + ") ";
    }
}
