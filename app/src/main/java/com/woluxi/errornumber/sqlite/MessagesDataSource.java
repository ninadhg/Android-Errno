/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber.sqlite;

/**
 * Created by ninadhg on 12/2/16.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.woluxi.errornumber.decode.ErrnoArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MessagesDataSource {
    private static final String TAG = "MsgDataSrc";

    private SQLiteDatabase database;
    private DiagnosticSQLiteHelper dbHelper;
    private String[] allColumns = {
            DiagnosticSQLiteHelper.COLUMN_ID,
            DiagnosticSQLiteHelper.COLUMN_VERSION,
            DiagnosticSQLiteHelper.COLUMN_DATA,
            DiagnosticSQLiteHelper.COLUMN_INFO,
            DiagnosticSQLiteHelper.WORKAROUND_URL,
            DiagnosticSQLiteHelper.WORKAROUND_INFO,
            DiagnosticSQLiteHelper.COLUMN_MODE,
            DiagnosticSQLiteHelper.COLUMN_DATETIME,
            DiagnosticSQLiteHelper.COLUMN_VENDOR_ICON_URL,
    };

    public MessagesDataSource(Context context) {
        dbHelper =  DiagnosticSQLiteHelper.getInstance(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }
    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public void updateMessage(Message msg){
        Log.d(TAG, "Update message "+ msg);
        ContentValues values = new ContentValues();
        values.put(DiagnosticSQLiteHelper.COLUMN_INFO, msg.getInfo());
        values.put(DiagnosticSQLiteHelper.WORKAROUND_URL, msg.getWorkaround_url());
        values.put(DiagnosticSQLiteHelper.WORKAROUND_INFO, msg.getWorkaround_info());
        values.put(DiagnosticSQLiteHelper.COLUMN_VENDOR_ICON_URL, msg.getVendor_icon_url());

        database.update(DiagnosticSQLiteHelper.TABLE_MESSAGES, values, "_id=" + msg.getId(), null);
    }

    public Message createMessage(Message msg){
        //// TODO: 12/8/16 : Check if the same message exists in the data base first.
        Log.d(TAG, "Input message "+ msg);
        ContentValues values = new ContentValues();
        values.put(DiagnosticSQLiteHelper.COLUMN_VERSION, 1);
        values.put(DiagnosticSQLiteHelper.COLUMN_VENDOR_ICON_URL, msg.getVendor_icon_url());
        values.put(DiagnosticSQLiteHelper.COLUMN_DATETIME, System.currentTimeMillis());

        String info = "";
        byte [] data = msg.getData();
        values.put(DiagnosticSQLiteHelper.COLUMN_DATA, data);
        int mode = msg.getMode();
        if(mode == 0 ){
            info = ErrnoArray.get(data[0]).toString();
            info = info + new String(Arrays.copyOfRange(data, 1, data.length));
        } else if (mode == 3 ) {
            info = new String(data);
        }
        values.put(DiagnosticSQLiteHelper.COLUMN_INFO,info);
        values.put(DiagnosticSQLiteHelper.COLUMN_MODE, mode);
        long insertId = database.insert(DiagnosticSQLiteHelper.TABLE_MESSAGES, null,
                values);
        Cursor cursor = database.query(DiagnosticSQLiteHelper.TABLE_MESSAGES,
                allColumns, DiagnosticSQLiteHelper.COLUMN_ID + " = " + insertId, null,
                null, null, null);
        cursor.moveToFirst();
        Message msgOut = cursorToMessage(cursor);
        cursor.close();
        return msgOut;
    }

    public void deleteMessage(Message msg) {
        long id = msg.getId();
        System.out.println("Comment deleted with id: " + id);
        database.delete(DiagnosticSQLiteHelper.TABLE_MESSAGES, DiagnosticSQLiteHelper.COLUMN_ID
                + " = " + id, null);
    }

    /*
        Delete single row from UserTable
    */
    void deleteRow(long id) {
        try {
            database.beginTransaction();
            database.execSQL("delete from " + DiagnosticSQLiteHelper.TABLE_MESSAGES + " where " +
                    DiagnosticSQLiteHelper.COLUMN_ID + " ='" + id + "'");
            database.setTransactionSuccessful();
        } catch (SQLException e) {
            Log.d(TAG, "Error while trying to delete  users detail");
        } finally {
            database.endTransaction();
        }
    }

    public Message getMessage(long id){
        Cursor c = database.query(DiagnosticSQLiteHelper.TABLE_MESSAGES,
                allColumns, DiagnosticSQLiteHelper.COLUMN_ID + " = " + id, null,
                null, null, null);
        c.moveToFirst();
        Message msg = cursorToMessage(c);
        c.close();
        return msg;
    }

    public List<Message> getAllMessages() {
        List<Message> messages = new ArrayList<Message>();

        Cursor cursor = database.query(DiagnosticSQLiteHelper.TABLE_MESSAGES,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Message msg = cursorToMessage(cursor);
            messages.add(msg);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return messages;
    }

    private Message cursorToMessage(Cursor cursor) {
        Message msg = new Message();
        msg.setId(cursor.getLong(0));
        msg.setVersion(cursor.getLong(1));
        msg.setData(cursor.getBlob(2));
        msg.setInfo(cursor.getString(3));
        msg.setWorkaround_url(cursor.getString(4));
        msg.setWorkaround_info(cursor.getString(5));
        msg.setMode(cursor.getInt(6));
        msg.setDatetime(cursor.getLong(7));
        msg.setVendor_icon_url(cursor.getString(8));
        return msg;
    }
}
