/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber.sqlite;

import android.util.Log;

import com.woluxi.scannerlib.data.Message.MessageRecord;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by ninadhg on 12/2/16.
 */

public class Message  implements Serializable {
    private long id;
    private long version;
    private String vendor_icon_url;
    private byte []data;
    private String info;
    private String workaround_url;
    private String workaround_info;
    private long datetime;

    public String getVendor_icon_url() {
        return vendor_icon_url;
    }

    public void setVendor_icon_url(String vendor_icon_url) {
        this.vendor_icon_url = vendor_icon_url;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    private int mode;

    public String getWorkaround_info() {
        return workaround_info;
    }

    public void setWorkaround_info(String workaround_info) {
        this.workaround_info = workaround_info;
    }

    private static final String TAG = "Message";

    public Message() {
    }

    final protected static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }
    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getWorkaround_url() {
        return workaround_url;
    }

    public void setWorkaround_url(String workaround_url) {
        this.workaround_url = workaround_url;
    }

    public Message(MessageRecord msg) {
        Log.d(TAG, " constructor msg " + msg);
        this.version = msg.getVersion();
        this.data = msg.getDataAsBytes();
        this.mode = msg.getMode();
        this.datetime = System.currentTimeMillis();
    }

    public byte[] getData() {
        return data;
    }

    public String getDataAsString() {
        return bytesToHex(data);
    }
    public void setData(byte[] data) {
        this.data = data;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", version=" + version +
                ", mode=" + mode +
                ", data=" + Arrays.toString(data) +
                ", info='" + info + '\'' +
                ", workaround_url='" + workaround_url + '\'' +
                '}';
    }
}
