/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by ninadhg on 12/2/16.
 */

public class DiagnosticSQLiteHelper extends SQLiteOpenHelper {
    public static final String TABLE_MESSAGES = "messages";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_VERSION = "version";
    public static final String COLUMN_VENDOR_ICON_URL = "vendor_icon_url";

    public static final String COLUMN_INFO = "info";
    public static final String WORKAROUND_URL = "workaround_url";
    public static final String WORKAROUND_INFO = "workaround_info";
    public static final String COLUMN_MODE = "mode";

    public static final String COLUMN_DATA = "data";
    public static final String COLUMN_DATETIME = "date";

    private static final String DATABASE_NAME = "messages.db";
    private static final int DATABASE_VERSION = 5;

    private static final String DATABASE_CREATE = "create table "
            + TABLE_MESSAGES + "( "
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_VERSION + " integer, "
            + COLUMN_VENDOR_ICON_URL + " text, "
            + COLUMN_DATA + " BLOB, "
            + COLUMN_INFO + " text, "
            + WORKAROUND_URL + " text, "
            + WORKAROUND_INFO + " text, "
            + COLUMN_MODE + " integer, "
            + COLUMN_DATETIME + " integer "
            + ");";

    private  DiagnosticSQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    private static DiagnosticSQLiteHelper dbHelper;

    public static synchronized DiagnosticSQLiteHelper getInstance(Context context){
        if(dbHelper == null) {
            dbHelper = new DiagnosticSQLiteHelper(context);
        }
        return dbHelper;
    }

    private  DiagnosticSQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(DiagnosticSQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESSAGES);
        onCreate(db);
    }
}
