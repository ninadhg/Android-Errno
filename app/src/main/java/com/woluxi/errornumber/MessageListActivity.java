/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;

public class MessageListActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    Context context;
    ImageButton backButton;
    RecyclerAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        recyclerView= (RecyclerView) findViewById(R.id.rv_messagelist);
        this.context = this.getApplicationContext();
        backButton = (ImageButton)findViewById(R.id.back_from_list);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        adapter=new RecyclerAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);

        //Layout manager for Recycler view

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onStart(){
        recyclerView.setAdapter(adapter);
        recyclerView.invalidate();
        super.onStart();
    }
}
