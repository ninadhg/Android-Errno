/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber.Async;

import android.app.Activity;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.widget.TextView;

import com.woluxi.errornumber.R;
import com.woluxi.errornumber.sqlite.Message;
import com.woluxi.errornumber.sqlite.MessagesDataSource;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ninadhg on 1/27/17.
 */

public class PostMessageData extends AsyncTask<String, Integer, String> {
    private static final String TAG = "PostMessage";

    Activity mActivity;
    long dbid;
    /*
    The constructor takes the database id and the activity name.
    The dbid is used to fetch the data from the database to send it to the backend server.
    The activity is used to get a context to set ui elements on successful data return.
     */
    public PostMessageData(Activity activity, long dbid) {
        mActivity = activity;
        this.dbid = dbid;
    }

    @Override
    protected String doInBackground(String... params) {
        HttpURLConnection urlConnection = null;

        BufferedReader reader = null;

        String JsonResponse;
        String JsonDATA = params[0];
        Log.d(TAG, "JsonDATA " + JsonDATA);
        try {
            URL url = new URL("http://woluxi.com/errno/?");

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setUseCaches(false);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            urlConnection.connect();

            DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
            wr.writeBytes(JsonDATA);
            wr.flush();
            wr.close();
            int responseCode = urlConnection.getResponseCode();
            Log.d(TAG, "responseCode " + responseCode);
            InputStream inputStream = urlConnection.getInputStream();
            //input stream
            StringBuffer buffer = new StringBuffer();
            if (inputStream == null) {
                // Nothing to do.
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String inputLine;
            while ((inputLine = reader.readLine()) != null)
                buffer.append(inputLine + "\n");
            if (buffer.length() == 0) {
                // Stream was empty. No point in parsing.
                return null;
            }
            JsonResponse = buffer.toString();
            Log.i(TAG,JsonResponse);
            return JsonResponse;
        } catch (IOException e) {
            e.printStackTrace();
        }
        finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (final IOException e) {
                    Log.e(TAG, "Error closing stream", e);
                }
            }
        }
        return null;
    }

    protected void onPostExecute(String result) {

        Log.d(TAG, "json return string " + result);
        if(result == null)
            return;
        JSONObject jObject;
        try {
            jObject = new JSONObject(result);
            TextView errText = (TextView)mActivity.findViewById(R.id.errorText);

            if(jObject != null) {
                MessagesDataSource messagesDataSource = new MessagesDataSource(mActivity.getApplicationContext());
                messagesDataSource.open();
                Message msg = messagesDataSource.getMessage(dbid);
                String html_err_Text = "<b>Error Details: </b>" + jObject.getString("display_string");
                errText.setText(Html.fromHtml(html_err_Text));
                msg.setInfo(jObject.getString("display_string"));
                messagesDataSource.updateMessage(msg);
                messagesDataSource.close();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }
    }
}