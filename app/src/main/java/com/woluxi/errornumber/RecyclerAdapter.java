/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.woluxi.errornumber.sqlite.Message;
import com.woluxi.errornumber.sqlite.MessagesDataSource;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by ninadhg on 12/2/16.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private MessagesDataSource messagesDataSource = null;
    private static final String TAG = "RecyclerAdapter";

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=inflater.inflate(R.layout.message_card, parent, false);
        messagesDataSource = new MessagesDataSource(context);

        RecyclerViewHolder viewHolder=new RecyclerViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        String vendor_text ;

        if(messagesDataSource == null)
            messagesDataSource = new MessagesDataSource(context);
        messagesDataSource.open();
        List<Message> msgs = messagesDataSource.getAllMessages();
        messagesDataSource.close();
        Message msg = msgs.get(position);
        Log.d(TAG,"message read in RecyclerAdapter " + msg );

        holder.msg = msg;

        String info = msg.getInfo();
        if(info.isEmpty()){
            if(msg.getMode() == 0)
                info = "0x" + msg.getDataAsString();
            else if (msg.getMode() == 3)
                info = new String(msg.getData());
        } else {
            int maxWidth = 50;
            if(info.length() > maxWidth) {
                int correctedMaxWidth = (Character.isLowSurrogate(info.charAt(maxWidth)))&&maxWidth>0 ? maxWidth-1 : maxWidth;
                System.out.println(info.substring(0, Math.min(info.length(), correctedMaxWidth)));

                info = info.substring(0, maxWidth) + "....";
            }
        }
        holder.error_text.setText(info);

        DateFormat df = new SimpleDateFormat("MMM dd, ''yy");
        String data = df.format(new Date(msg.getDatetime()));
        holder.date_text.setText(data);

        DateFormat tf = new SimpleDateFormat("HH:mm");
        String time = tf.format(new Date(msg.getDatetime()));
        holder.time_text.setText(time);


    }

    View.OnClickListener clickListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            RecyclerViewHolder vholder = (RecyclerViewHolder) v.getTag();
            int position = vholder.getPosition();
            Log.d(TAG, "clicked position " + position);
            Toast.makeText(context,"This is position "+position, Toast.LENGTH_LONG ).show();

        }
    };

    @Override
    public int getItemCount() {
        if(messagesDataSource == null)
            messagesDataSource = new MessagesDataSource(context);

        messagesDataSource.open();
        List<Message> msgs = messagesDataSource.getAllMessages();
        messagesDataSource.close();
        return msgs.size();
    }


    public RecyclerAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

}
