/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.woluxi.errornumber.sqlite.Message;


/**
 * Created by ninadhg on 12/2/16.
 */

public class RecyclerViewHolder extends RecyclerView.ViewHolder {
    TextView error_text;
    TextView date_text;
    TextView time_text;

    public View view;
    int currentItem ;
    public Message msg;
    private static final String TAG = "RecyclerViewHolder";
    private final Context context;

    public RecyclerViewHolder(View itemView) {
        super(itemView);
        view = itemView;
        context = itemView.getContext();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "clicked position " + msg );
                Intent intent = new Intent(context, MessageDetailActivity.class);
                intent.putExtra("DBID",msg.getId());
                Log.d(TAG, "Adding id " + msg.getId());
                context.startActivity(intent);
            }
        });

        error_text = (TextView) itemView.findViewById(R.id.card_error_text);
        date_text = (TextView) itemView.findViewById(R.id.card_date_text);
        time_text = (TextView) itemView.findViewById(R.id.card_time_text);
    }

    public void onBindViewHolder(RecyclerViewHolder viewHolder, int i) {
        viewHolder.currentItem = i;
    }
}
