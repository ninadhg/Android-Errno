/*
 *
 *  * Copyright (C) Woluxi, Inc - All Rights Reserved
 *  * Unauthorized copying of this file, via any medium is strictly prohibited
 *  * Proprietary and confidential
 *  * Written by Ninad Ghodke<ninad@woluxi.com>,2017
 *
 */

package com.woluxi.errornumber;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.woluxi.errornumber.Async.PostMessageData;
import com.woluxi.errornumber.sqlite.Message;
import com.woluxi.errornumber.sqlite.MessagesDataSource;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MessageDetailActivity extends AppCompatActivity {
    private static final String TAG = "MsgDetAct";
    ImageButton backButton;
    ImageButton deleteButton;
    Context context;
    Message dmsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        long s = getIntent().getLongExtra("DBID", 100000);

        super.onCreate(savedInstanceState);
        setContentView(R.layout.message_detail);
        this.context = this.getApplicationContext();

        MessagesDataSource messagesDataSource = new MessagesDataSource(context);
        messagesDataSource.open();
        this.dmsg = messagesDataSource.getMessage(s);
        messagesDataSource.close();

        backButton = (ImageButton) findViewById(R.id.backButtonFromDetail);
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                finish();
            }
        });
        deleteButton = (ImageButton) findViewById(R.id.delete_from_detail);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MessagesDataSource messagesDataSource = new MessagesDataSource(context);
                messagesDataSource.open();
                messagesDataSource.deleteMessage(dmsg);
                messagesDataSource.close();
                onBackPressed();
            }
        });
        TextView errText = (TextView)findViewById(R.id.errorText);
        TextView dateTimeText = (TextView)findViewById(R.id.detail_date_and_time);

        Log.d(TAG,"The dbid got is " + s);
        Log.d(TAG,"The entire extra " + getIntent());
        messagesDataSource.close();

        String post_dict = getIntent().getStringExtra("jsondata");
        if(post_dict != null) {
            new PostMessageData(this, s).execute(String.valueOf(post_dict));
        }

        //check if the message is already decoded
        String info = dmsg.getInfo();
        if(info.isEmpty()){
            if(dmsg.getMode() == 0)
                info = "0x" + dmsg.getDataAsString();
            else if (dmsg.getMode() == 3)
                info = new String(dmsg.getData());
        }
        String html_err_Text = "<b>Error Details:</b> " + info ;
        assert errText != null;
        errText.setText(Html.fromHtml(html_err_Text));


        DateFormat df = new SimpleDateFormat("MMM d, yyyy HH:mm");
        String data = df.format(new Date(dmsg.getDatetime()));
        dateTimeText.setText(data);
    }
}
