package com.woluxi.errornumber;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.woluxi.errornumber.sqlite.Message;
import com.woluxi.errornumber.sqlite.MessagesDataSource;
import com.woluxi.scannerlib.activity.CameraPreview;
import com.woluxi.scannerlib.data.Message.CurrentStateEnum;
import com.woluxi.scannerlib.data.Message.MessageRecord;
import com.woluxi.scannerlib.data.Message.MessageStateMachine;
import com.woluxi.scannerlib.data.MessageStateMachineCallback;
import com.woluxi.scannerlib.data.MessageStateMachineController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.TimeZone;

public class CameraActivity extends AppCompatActivity implements MessageStateMachineCallback {
    private CameraPreview camPreview;
    private FrameLayout mainLayout;
    ProgressBar progressBar;
    TextView lbl;
    FloatingActionButton listButton;
    String intentInputData = "";
    private static final String TAG = "CamActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_camera);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        }
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyhavePermission()) {
                requestForPermission();
            }
        }
        startAct();
    }

    void handleSendText(Intent intent) {
        intentInputData = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (intentInputData != null) {
            Log.d(TAG," the shared text ID is " + intentInputData);
            /*
            // There is no point in parsing this here and passing it on to the backend..
            // instead we will pass the entire string to the backend which is more flexible.
            JSONObject jObject;
            String random_uuid = null;
            String auth_id = null;
            try {
                jObject = new JSONObject(intentInputData);
                if(jObject.has("random_uuid")) {
                    random_uuid = jObject.getString("random_uuid");
                }
                if(jObject.has("auth_id")) {
                    auth_id = jObject.getString("auth_id");
                }
                Log.d(TAG,"got json " + random_uuid + " " + auth_id);
            } catch (JSONException e){
                e.printStackTrace();
            }
            */
        }
    }

    private boolean checkIfAlreadyhavePermission() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (result != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;

    }

    private void requestForPermission() {
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 101);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startAct();
                } else {
                    finish();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    void startAct(){
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();
        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }

        SurfaceView camView = new SurfaceView(this);
        SurfaceHolder camHolder = camView.getHolder();
        camPreview = new CameraPreview(getApplicationContext());

        camHolder.addCallback(camPreview);
        camHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);;

        progressBar.setProgress(0);
        progressBar.setMax(100);
        lbl = (TextView) findViewById(R.id.textView);
        lbl.setText("");
        final Handler myHandler = new Handler();
        new Thread(new Runnable() {
            public void run(){
                while (true) {
                    myHandler.postDelayed(showBlinkRate, 0);
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        MessageStateMachineController.INSTANCE.registerCallback(this);

        listButton = (FloatingActionButton) findViewById(R.id.message_list_button);
        listButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MessageListActivity.class);
                startActivity(intent);
            }
        });

        mainLayout = (FrameLayout) findViewById(R.id.activity_camera);
        mainLayout.addView(camView);
        MessageStateMachine.INSTANCE.setMetaDataOptional(true);
    }

    MessageStateMachine msgRead = MessageStateMachine.INSTANCE;
    static String [] unableToRead = { "Center on the blinking led",
            "Move the phone closer to led",
            "Hold the phone steady"};
    int unableToReadCounter = 0;
    private Runnable showBlinkRate = new Runnable()
    {
        @Override
        public void run()
        {
            if(msgRead.getState() == CurrentStateEnum.INITIALIZED){
                lbl.setText(unableToRead[(unableToReadCounter++/10) % 3]);
                progressBar.setProgress(0);
                return;
            }

            switch (msgRead.getState()){
                case PREAMBLE_COMPLETE:
                    lbl.setText("Starting data acquisition");
                    break;
                case METADATA_COMPLETE:
                    lbl.setText("MetaData read");
                    break;
                case DATA_FIRST_BYTE_READ:
                    lbl.setText("Started reading data");
                    break;
                case DATA_COMPLETE:
                    lbl.setText("Reading data completed");
                    break;
            }
        }
    };

    @Override
    public void initComplete() {
        progressBar.setProgress(0);
    }

    @Override
    public void dataFirstByteRead() {

    }

    @Override
    public void preambleReadComplete() {
        progressBar.setProgress(30);
    }

    @Override
    public void metaDataReadComplete() {
        progressBar.setProgress(60);
    }

    @Override
    public void vendorReadComplete() {
    }

    @Override
    public void hwMsgIdReadComplete() {

    }

    @Override
    public void dataReadComplete(MessageRecord msg) {
        progressBar.setProgress(100);
        final Context context = this;
        MessageRecord br = msgRead.getRecord();
        Message msgSql = new Message(br);
        MessagesDataSource messagesDataSource = new MessagesDataSource(context);
        messagesDataSource.open();
        msgSql = messagesDataSource.createMessage(msgSql);
        messagesDataSource.close();
        //Log.d(TAG, "Added msg " + msgSql);
        Intent intent = new Intent(context, MessageDetailActivity.class);
        //Log.d(TAG, "Adding id " + msgSql.getId());
        JSONObject post_dict = new JSONObject();
        try {
            post_dict.put("message", Base64.encodeToString(br.getDataAsBytes(), Base64.DEFAULT));
            post_dict.put("version", 0);
            post_dict.put("mode", br.getMode());
            post_dict.put("AppInputParams", intentInputData);
            post_dict.put("device_record_time", msgSql.getDatetime());
            String dev_time_zone = TimeZone.getDefault().getID().replace("\\","");
            post_dict.put("device_time_zone", dev_time_zone);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        intent.putExtra("DBID", msgSql.getId());
        intent.putExtra("jsondata", String.valueOf(post_dict));
        startActivity(intent);
    }

}
